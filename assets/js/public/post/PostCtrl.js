/**
 * Created by dastko on 8/5/15.
 */
angular.module('PostMod').controller('PostCtrl', ['$scope', '$http', 'toastr', function ($scope, $http, toastr) {
  console.log('Post Controller initialized...');

  $scope.getCurrentUser = function () {
    $http.get('/getuser')
      .then(function onSuccess(user) {
        $scope.user = user.data;
        $scope.useremail = user.data.email;
      })
      .catch(function onError(err) {
        console.log(err);
      })
  },

    $scope.addPost = function () {

      $http.post('/addpost', {
        title: $scope.title,
        content: $scope.content,
        subtitle: $scope.subtitle,
        picture: $scope.picture,
        useremail: $scope.useremail
      })
        .then(function onSuccess(response) {
          window.location = '#/addpost';
          toastr.info('Post added successfully', "Post", {
            closeButton: true
          });
          return;
        })
        .catch(function onError(err) {
          if (err.status == 400 || 404) {
            toastr.error('Invalid Format, please check your Email', "devError", {
              closeButton: true
            });
            return;
          }
          toastr.error('An error has occured, please try again', "devError", {
            closeButton: true
          });
          return;
        });

      $http.get('/post').then(function (response) {
        console.log(response);
        $scope.posts = response.data;
      })

    }

}])
