/**
 * Created by dastko on 8/6/15.
 */
var mainApp = angular.module('mainApp', ['ngRoute', 'toastr', 'SignupMod', 'LoginMod', 'PostMod', 'DashboardMod']);

mainApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/login', {
        templateUrl: '../templates/login.html',
        controller: 'LoginCtrl'
      }).
      when('/signup', {
        templateUrl: '../templates/signup.html',
        controller: 'SignupCtrl'
      }).
      when('/posts', {
        templateUrl: '../templates/posts.html',
        controller: 'PostGetCtrl'
      }).
      when('/addpost', {
        templateUrl: '../templates/addpost.html',
        controller: 'PostCtrl',
        access : isLogged = true
      }).
      when('/post', {
        templateUrl: '../templates/post.html',
        controller: 'PostGetCtrl'
      }).
      when('/dashboard', {
        templateUrl: '../templates/dashboard.html',
        controller: 'DashboardCtrl',
        access: {
          requiresLogin: true
        }
      }).

      otherwise({
        redirectTo: '/posts'
      });
  }]);



