angular.module('SignupMod').controller('SignupCtrl', ['$scope', '$http', 'toastr', function ($scope, $http, toastr) {
    console.log('Signup Controller initialized...');

    $scope.signupSubmit = function () {
        console.log($scope.name);
        console.log($scope.email);
        console.log($scope.password);

        $http.post('/signup', {
            name: $scope.name,
            email: $scope.email,
            password: $scope.password
        })
            .then(function onSuccess(response) {
                window.location = '#/login';
                toastr.success("Please login to your dashboard!", "Successfully signed up");
            })
            .catch(function onError(err) {
                if (err.status == 400 || 404) {
                    toastr.error('Invalid Format, please check your Email', "devError", {
                        closeButton: true
                    });
                    return;
                }
                toastr.error('An error has occured, please try again', "devError", {
                    closeButton: true
                });
                return;
            })
    }
}])
