/**
 * Created by dastko on 8/5/15.
 */
angular.module('PostMod').controller('PostGetCtrl', ['$scope', '$http', function ($scope, $http) {
  console.log('Post Controller initialized...');

  $http.get('/post').then(function (response) {
    console.log(response);
    $scope.posts = response.data;
  }),

    $scope.showPost = function (post) {

      $scope.showP = true;
      $scope.title = post.title;
      $scope.content = post.content;
      $scope.subtitle = post.subtitle;
      $scope.picture = 'img/post-bg.jpg';
      $scope.useremail = post.useremail;
      $scope.createdAt = post.createdAt;
    },

    $scope.hideOnePost = function (){
      $scope.showP = false;
      $scope.picture = 'img/home-bg.jpg';
    }
}])
