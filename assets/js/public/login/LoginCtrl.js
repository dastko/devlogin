angular.module('LoginMod').controller('LoginCtrl', ['$scope', '$http', 'toastr', function ($scope, $http, toastr) {

    $scope.loginSubmit = function () {
        $http.post('/login', {
            email: $scope.email,
            password: $scope.password
        })
            .then(function onSuccess() {
                window.location = '/dashboard';
            })
            .catch(function onError(err) {
                if (err.status == 400 || 404) {
                    toastr.error('Invalid Username/Password', "devError", {
                        closeButton: true
                    });
                    return;
                }
                toastr.error('An error has occured, please try again', "devError", {
                    closeButton: true
                });
                return;
            })
    }
}])
